/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solidstuff.sacrificexsteel.htl22maps.WebuntisAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author steel
 */
public class APIConnector {
    public static final int MAJOR_VERSION=1;
    public static final int MINOR_VERSION=1;
    private final static String SERVER="http://webuntis.ignorelist.com";

    public static HashMap<Integer,String> getAllIDs(String object){
        try {
            URL url=new URL(SERVER+"/API/index.php/"+object+"/getallids");
            URLConnection conn = url.openConnection();
            InputStream in=conn.getInputStream();
            int read;
            String data="";
            while (((read=in.read())!=-1)){
                data+=(char)read;
            }
            JSONObject obj= new JSONObject(data);
            if (obj.getInt("status")==200) {
                JSONArray dataArr = obj.getJSONArray("data");
                HashMap<Integer, String> rt=new HashMap<Integer, String>();
                int size=dataArr.length();
                for (int i=0;i<size;i++){
                    JSONObject current=(JSONObject)dataArr.get(i);
                    String name=current.getString("name");
                    int id=current.getInt("ID");
                    rt.put(id,name);
                }
                return rt;
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(APIConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(APIConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static Day getTimetableByID(String object,int id){
        try {
            URL url=new URL(SERVER+"/API/index.php/"+object+"/getbyid/:"+id);
            URLConnection conn = url.openConnection();
            InputStream in=conn.getInputStream();
            int read;
            String response="";
            while (((read=in.read())!=-1)){
                response+=(char)read;
            }
            JSONObject obj= new JSONObject(response);
            if (obj.getInt("status")==200) {
                JSONArray data = obj.getJSONArray("data");
                Day day = new Day();
                int size = data.length();
                for (int i = 0; i < size; i++) {
                    Hour hour = new Hour();
                    JSONObject hourData = (JSONObject) data.get(i);
                    String starttime = hourData.getString("starttime");
                    String endtime = hourData.getString("endtime");
                    String className = hourData.getString("className");
                    String subject = hourData.getString("suName");
                    if (!hourData.isNull("teName")){
                        JSONArray teachers = hourData.getJSONArray("teName");
                        ArrayList<String> tmp = new ArrayList<String>();
                        int tmpSize = teachers.length();
                        for (int x = 0; x < tmpSize; x++) {
                            tmp.add(teachers.getString(x));
                        }
                        hour.setTeachers(tmp);
                    }
                    if (!hourData.isNull("roName")){
                        JSONArray rooms = hourData.getJSONArray("roName");
                        ArrayList<String> tmp = new ArrayList<String>();
                        int tmpSize = rooms.length();
                        for (int x = 0; x < tmpSize; x++) {
                            tmp.add(rooms.getString(x));
                        }
                        hour.setRooms(tmp);
                    }

                    hour.setStartTime(starttime);
                    hour.setEndTime(endtime);
                    hour.setClassName(className);
                    hour.setSubjectName(subject);

                    day.getHours().add(hour);
                }
                return day;
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(APIConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(APIConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static Day getTimetableByName(String object,String name){
        try {
            URL url=new URL(SERVER+"/API/index.php/"+object+"/getbyname/:"+name);
            URLConnection conn = url.openConnection();
            InputStream in=conn.getInputStream();
            int read;
            String response="";
            while (((read=in.read())!=-1)){
                response+=(char)read;
            }
            JSONObject obj= new JSONObject(response);
            if (obj.getInt("status")==200) {
                JSONArray data = obj.getJSONArray("data");
                Day day = new Day();
                int size = data.length();
                for (int i = 0; i < size; i++) {
                    Hour hour = new Hour();
                    JSONObject hourData = (JSONObject) data.get(i);
                    String starttime = hourData.getString("starttime");
                    String endtime = hourData.getString("endtime");
                    String className = hourData.getString("className");
                    String subject = hourData.getString("suName");
                    if (!hourData.isNull("teName")){
                        JSONArray teachers = hourData.getJSONArray("teName");
                        ArrayList<String> tmp = new ArrayList<String>();
                        int tmpSize = teachers.length();
                        for (int x = 0; x < tmpSize; x++) {
                            tmp.add(teachers.getString(x));
                        }
                        hour.setTeachers(tmp);
                    }
                    if (!hourData.isNull("roName")){
                        JSONArray rooms = hourData.getJSONArray("roName");
                        ArrayList<String> tmp = new ArrayList<String>();
                        int tmpSize = rooms.length();
                        for (int x = 0; x < tmpSize; x++) {
                            tmp.add(rooms.getString(x));
                        }
                        hour.setRooms(tmp);
                    }

                    hour.setStartTime(starttime);
                    hour.setEndTime(endtime);
                    hour.setClassName(className);
                    hour.setSubjectName(subject);

                    day.getHours().add(hour);
                }
                return day;
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(APIConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(APIConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
