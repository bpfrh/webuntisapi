package com.solidstuff.sacrificexsteel.htl22maps.WebuntisAPI;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by steel on 26.02.15.
 */
public class Hour {
    //private HashMap<Integer,String> teachers=new HashMap<Integer, String>();
    //private HashMap<Integer,String> rooms=new HashMap<Integer, String>();
    private ArrayList<String> teachers = new ArrayList<String>();
    private ArrayList<String> rooms = new ArrayList<String>();
    private String subjectName="";
    private String className="";
    private String startTime="";
    private String endTime="";

    /*public HashMap<Integer, String> getTeachers() {
        return teachers;
    }

    public void setTeachers(HashMap<Integer, String> teachers) {
        this.teachers = teachers;
    }
     public HashMap<Integer, String> getRooms() {
        return rooms;
    }

    public void setRooms(HashMap<Integer, String> rooms) {
        this.rooms = rooms;
    }*/

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public ArrayList<String> getTeachers() {
        return teachers;
    }

    public void setTeachers(ArrayList<String> teachers) {
        this.teachers = teachers;
    }

    public ArrayList<String> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<String> rooms) {
        this.rooms = rooms;
    }
}
